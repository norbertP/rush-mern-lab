const { callAPI } = require('./api');

export {
    signIn,
    signOut
}



function signOut() {
    return callAPI('auth/signout', 'GET', {})
   
}

function signIn({ email, password }) {
    return callAPI('auth/signin', 'POST', { email, password } )
}