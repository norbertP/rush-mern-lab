const backendApi = "http://localhost:4000"


export function callAPI(uri, method, data) {

    console.log("API called", data);
    var myInit = {
        method: method,
        headers: { 'Content-Type': 'application/json' }
    };
    if (method === "POST" || method === "PUT")
        myInit.body = JSON.stringify(data)

    const request = new Request(`${backendApi}/${uri}`, myInit);
    return fetch(request, myInit)
        .then(res => res.json())
        .catch(error => console.log("Error in CallAPI", error))

}


