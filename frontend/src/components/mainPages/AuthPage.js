import React, { useState } from "react";
import { Button, FormGroup, FormControl, FormLabel } from "react-bootstrap";
import Alert from 'react-bootstrap/Alert'
import "./Auth.css";
import * as constants from "../../constants/actions";


export {
  SignIn,
  SignOut,
  CreateAccount
}

function CreateAccount(props)
{
  return ("Création de compte");
}

function SignOut(props)
{
  props.onAuthActionClick(constants.SIGNOUT, {});
}

function SignIn(props) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  
  function validateForm() {
    return email.length > 0 && password.length > 0;
  }

  async function handleSubmit(event) {
    event.preventDefault();
    props.onAuthActionClick(constants.SIGNIN, { email, password });
  }

  return (
    <div className="Login">
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="email" >
          <FormLabel>Email</FormLabel>
          <FormControl
            autoFocus
            type="email"
            value={email}
            onChange={e => setEmail(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="password" >
          <FormLabel>Password</FormLabel>
          <FormControl
            value={password}
            onChange={e => setPassword(e.target.value)}
            type="password"
          />
        </FormGroup>
        <Button block disabled={!validateForm()} type="submit">
          Login
        </Button>
      </form>
      <br/>
      {props.signInError && <Alert variant="danger">{props.signInError}</Alert>}
    </div>
  );
}

