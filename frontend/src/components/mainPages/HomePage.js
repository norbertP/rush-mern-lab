import React from 'react';
import StaticPageHeader from '../sub/StaticPageHeader';
import TweetCreation from '../TweetCreation';

export function HomePage() {
    return (
        <div>
        <StaticPageHeader title="Home"/>
       
        <TweetCreation/>

        </div>
    )
}