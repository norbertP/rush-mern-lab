import React from 'react';
import './StaticPageHeader.css';

export default function StaticPageHeader (props){
    return (<div className="staticHeader">{props.title}</div>)
}