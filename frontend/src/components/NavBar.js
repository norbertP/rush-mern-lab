import React from 'react';
import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import Form from 'react-bootstrap/Form'
import FormControl from 'react-bootstrap/FormControl'
import Button from 'react-bootstrap/Button'
import {
  Link
} from "react-router-dom";

var FontAwesome = require('react-fontawesome');

export function NavBar() {
  return (
    <Navbar bg="light" variant="light">
      <Navbar.Brand>

      </Navbar.Brand>
      <Navbar.Collapse>
        <Nav justify='right'>
          <Nav.Link><Link to="/sign-in">Sign In</Link>
          </Nav.Link>


          <Nav.Link><Link to="/create-account">Create account</Link>
          </Nav.Link>


          <Nav.Link><Link to="/sign-out">Sign Out</Link>
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}


export function NavBar2() {
  return (

    <Navbar bg="light" variant="light">
      <Navbar.Brand>My twitter</Navbar.Brand>
      <Nav className="mr-auto">
        <Nav.Link><Link to="/"><FontAwesome name='fas fa-home' size="2x"
          style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}></FontAwesome></Link>
        </Nav.Link>

        <Nav.Link><Link to="/sign-in">Sign In</Link>
        </Nav.Link>


        <Nav.Link><Link to="/create-account">Create account</Link>
        </Nav.Link>


        <Nav.Link><Link to="/sign-out">Sign Out</Link>
        </Nav.Link>


        <Nav.Link><Link to="/search"><FontAwesome name='fas fa-search' size="2x"
          style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}></FontAwesome></Link></Nav.Link>
        <Nav.Link><Link to="/member"><FontAwesome name='fas fa-user' size="2x"
          style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}></FontAwesome></Link></Nav.Link>
      </Nav>
      <Form inline>
        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
        <Button variant="outline-primary">Search</Button>
      </Form>
    </Navbar>

  )
}