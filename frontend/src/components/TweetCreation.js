import React from 'react';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Avatar from 'react-avatar';
import me from '../images/avatars/me.png';
const FontAwesome = require('react-fontawesome');



export default function TweetCreation() {
  return (
    <div className="row border mt-3" >
      <div className="col-sm-2 pt-2 justify-content-center">
        <Avatar size="50" twitterHandle="sitebase" src={me} />
      </div>
      <div className="col-sm-10 pt-1 border">
        <Form.Group controlId="TweetCreation.message" className="mt-2 border">
          <Form.Control as="textarea" rows="3" placeholder="What's happeing ?" />
        </Form.Group>
        <div className="d-flex row ">
          <div className="d-flex col-sm-12 pt-1 pb-1 border align-items justify-content-between align-items-center">
            <div>
              <FontAwesome name='fas fa-picture-o' className="btn-primary stretched-link" size="1x" border="true"></FontAwesome>
              <FontAwesome name='fas fa-smile-o' className="btn-primary stretched-link ml-3 " size="1x" border="true"></FontAwesome>
              <FontAwesome name='fas fa-picture-o' className="btn-primary stretched-link ml-3" size="1x" border="true"></FontAwesome>

            </div>
            <div>
              <Button as="sucess" > Tweet</Button>
            </div>
          </div>
        </div>
      </div>


    </div>
  )
}