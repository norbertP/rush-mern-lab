import React from 'react';
import {
    Switch,
    Route
  } from "react-router-dom";
const {HomePage, MemberPage, SearchPage} = require('./mainPages');
const {SignIn, SignOut, CreateAccount} = require('./mainPages');

export function Main(props) {
    return (

          <Switch>
            <Route exact path="/">
              <HomePage />
            </Route>
            <Route path="/search">
              <SearchPage />
            </Route>
            <Route path="/member">
              <MemberPage />
            </Route>
            <Route path="/sign-in" >
              <SignIn logged={props.logged} authError={props.authError} onAuthActionClick={props.onAuthActionClick}/>
            </Route>
            <Route path="/sign-out">
              <SignOut onAuthActionClick={props.onAuthActionClick}/>
            </Route>
            <Route path="/create-account">
              <CreateAccount authError={props.authError} onAuthActionClick={props.onAuthActionClick}/>
            </Route>
          </Switch>

    )
}