import * as constants from "./constants/actions";
import React, {useHistory} from "react";
import {
  BrowserRouter as Router
} from "react-router-dom";
const { NavBar, Main } = require("./components");
const { signIn, signOut } = require("./services/auth.service");


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      logged: false,
      authError: ""
    };

  }

  onAuthAction(action, data) {
    //const history = useHistory();

    switch (action) {
      case constants.SIGNIN: {
        const { email, password } = data;
        console.log('Le this', this);
        signIn({ email, password })
          .then(result => {
            if (result.code) {
              this.setState({
                authError: result.message,
                logged: false
              });
            }
            else {
              this.setState({
                authError: undefined,
                logged: true
              });
              //history.push("/")
            }
          }
          )
          .catch(console.log)
          break;
      }
      case constants.SIGNOUT: {
        signOut()
          .then();//history.push("/"));
          break;
      }
      case constants.CREATE_ACCOUNT: {
        break;
      }
      default: { }

    }
}

render(props) {
  return (
    <div className="container">
      <Router>
        <NavBar />
        <Main logged={this.state.logged} authError={this.state.authError} onAuthActionClick={this.onAuthAction} />
      </Router>
    </div>
  );
}
}

export default App;