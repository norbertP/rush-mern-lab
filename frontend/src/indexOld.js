import React from 'react';
import ReactDOM from 'react-dom';
import App from './AppOld';

// Importing Sass with Bootstrap CSS
import './App.scss';

ReactDOM.render(<App />, document.getElementById('root'));
