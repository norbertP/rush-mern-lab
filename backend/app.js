var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var authRouter = require('./routes/auth');

var firebase = require('firebase/app');
const cors = require('cors');
require('firebase/auth');
require('firebase/database');


var app = express();

const firebaseConfig = {
  apiKey: "AIzaSyDXfGITtuv-hxL5vIQRBjdrE2FyGzBIpd8",
  authDomain: "mern-rush.firebaseapp.com",
  databaseURL: "https://mern-rush.firebaseio.com",
  projectId: "mern-rush",
  storageBucket: "mern-rush.appspot.com",
  messagingSenderId: "723910325994",
  appId: "1:723910325994:web:ce5c34b32f4d376e160993",
  measurementId: "G-0GLVTEPM75"
};
 
firebase.initializeApp(firebaseConfig);


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(cors());
app.use(express.json());
app.use((req, res, next) => {console.log("received : ", req.body); next()});
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/auth', authRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
