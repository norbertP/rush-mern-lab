const firebase = require('firebase');

module.exports = {
  createNewUser,
  signInUser,
  signOutUser
}


function createNewUser(email, password) {
    return new Promise(
      (resolve, reject) => {
        firebase.auth().createUserWithEmailAndPassword(email, password).then(
          () => {
            resolve();
          },
          (error) => {
            reject(error);
          }
        );
      }
    );
}

function signInUser(email, password) {
  return new Promise(
    (resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(email, password).then(
        () => {
          const observable = firebase.auth().onAuthStateChanged;
          resolve(observable );
          /*
          objservable = firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
              console.log("User connected !!!", user)
            } else {
              // No user is signed in.
              console.log("Observable User Disconnected !!!")
            }
          });*/


        },
        (error) => {
          reject(error);
        }
      );
    }
  );
}

function signOutUser() {
  console.log("Request User Disconnected !!!")
  return firebase.auth().signOut();
}

