var express = require('express');
const {createNewUser, signInUser, signOutUser} = require('../services/auth.service');

var router = express.Router();

/* Create a user account */
router.post('/create', function(req, res, next) {
  createNewUser(req.body.email, req.body.password).then(
    () => {
      res.status(201).json("User created");
    },
    (error) => {
      res.status(400).json(error);
    }
  );
});

/* Sign/Log in a user */
router.post('/signin', function(req, res, next) {
  signInUser(req.body.email, req.body.password).then(
    () => {
      res.status(200).json("User log in !");
    },
    (error) => {
      res.status(400).json(error);
    }
  );
});

/* Sign/Log out a user */
router.get('/signout', function(req, res, next) {
  signOutUser().then(
    () => {
      res.status(200).json("User sign out");
    },
    (error) => {
      res.status(400).json(error);
    }
  );
});





module.exports = router;
