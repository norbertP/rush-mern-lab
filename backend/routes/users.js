var express = require('express');
var router = express.Router();
var Rx = require('rxjs');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/:id', (req, res) => {
  return getUserById(req.params.id)
    .toPromise()/* return the Rx stream as promise to express so it traces its lifecycle */
    .then(
      user => res.send(user),
      err => res.status(500).send(err.message)
    );
});    

function getUserById(id) {
  // stub implementation
  return Rx.Observable.of({ id, name: 'username' }) 
    .delay(100);
}
module.exports = router;
